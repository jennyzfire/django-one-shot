from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

    context_object_name = "todo_lists"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

    context_object_name = "todolist_detail"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    context_object_name = "creating"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args={self.object.pk})


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    context_object_name = "editing"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args={self.object.pk})


def delete_view(request, pk):
    model_instance = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/itemcreate.html"
    fields = ["task", "due_date", "is_completed", "list"]

    context_object_name = "adding_item"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args={self.object.list.pk})


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/itemupdate.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args={self.object.list.pk})
